<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');
echo "<!DOCTYPE html> 
      <html lang='vn'> 
      <head><meta charset='UTF-8'></head> 
      <title>Register</title>
      <body>\n";

echo "<fieldset style='width: 600px; height: 400px; border:#2980b9 solid'>";

echo "<form style = 'margin: 50px 70px 0 50px'>
        <table style = 'border-collapse:separate; border-spacing:20px 20px;'>
        <tr height = '40px'>
            <td width = 40% style = 'background-color: #2980b9; 
            vertical-align: top; text-align: left; padding: 15px 15px'>
                <label style='color: white;'>Họ và tên</label>
            </td>
            <td width = 40% >
                <input type='text' style = 'line-height: 32px; border-color:#2980b9'>
            </td>
        </tr>
        <tr height = '40px'>
            <td width = 40% style = 'background-color: #2980b9; 
            vertical-align: top; text-align: left; padding: 15px 15px'>
                <label style='color: white;'>Giới tính</label>
            </td>
            <td width = 40% >";
                $gender = array(0,1);
                foreach ($gender as $x) {
                    if ($x == 0) {
                        echo "<label class='container'>
                                <input type='radio' name='radio'>
                                Nam
                            </label>";
                    }
                    elseif ($x == 1) {
                        echo "<label class='container'>
                                <input type='radio' checked='checked' name='radio'>
                                Nữ
                            </label>";
                    }
                }
            echo"</td>
        </tr>
        <tr height = '40px'>
            <td style = 'background-color: #2980b9; 
            vertical-align: top; text-align: left; padding: 15px 15px'>
                <label style='color: white;'>Phân Khoa</label>
            </td>
            <td height = '40px'>
                <select style = 'border-color:#2980b9;height: 100%;width: 80%;'>
                    <option ></option>";
                    $facultyLabel = array("MAT", "KDL");
                    foreach ($facultyLabel as $i) {
                        if ($i == "MAT") {
                            echo "<option value = 'MAT'> Khoa học máy tính </option>";
                        }
                        elseif ($i == "KDL") {
                            echo "<option value = 'KDL'> Khoa học vật liệu </option>";
                        }
                    }
                echo"</select>
            </td>
        </tr>
        </table>
        <button style='background-color: #27ae60; border-radius: 10px; 
        width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Đăng Kí</button>
</form>
</fieldset>
</body>\n";
echo "</html>";